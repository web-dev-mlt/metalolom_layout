module.exports = ({ addComponents, theme }) => {
  const { mediaText, screens, spacing } = theme('gutenberg');
  const specialAlignments = {
    '.wp-block-image.alignfull, .wp-block-image.alignwide': {
      paddingLeft: 0,
      paddingRight: 0,
      marginLeft: 'auto',
      marginRight: 'auto',

      'img': {
        width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
  }

  const alignLeft = {
    '.wp-block-image .alignleft': {

      [`@media (min-width: ${screens.sm})`]: {
        float: 'inherit',
        maxWidth: '100%',

        'img': {
          width: '100%',
          marginBottom: spacing.vertical.default

        },
      },

      [`@media (min-width: ${screens.md})`]: {
        float: 'left',
        maxWidth: '50%',

        'img': {
          width: '100%',
          marginTop: `calc(${spacing.vertical.default} / 2)`,
          marginBottom: 0,
          paddingRight: spacing.horizontal,
        },
      },

    },
  }

  const alignRight = {
    '.wp-block-image .alignright': {


      [`@media (min-width: ${screens.sm})`]: {
        float: 'inherit',
        maxWidth: '100%',

        'img': {
          width: '100%',
          marginBottom: spacing.vertical.default,
        },
      },


      [`@media (min-width: ${screens.md})`]: {
        float: 'right',
        maxWidth: '50%',

        'img': {
          width: '100%',
          marginBottom: 0,
          paddingLeft: spacing.horizontal,
        },
      },
    },
  }

  addComponents([
    alignLeft,
    alignRight,
  ])

  theme('gutenberg.supports.wideAlignments')
    && addComponents(specialAlignments)
}
