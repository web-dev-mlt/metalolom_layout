$('.partners-items').slick({
    dots: false,
    infinite: true,
    lazyLoad: 'ondemand',
    speed: 300,
    arrows: false,
    slidesToShow: 8,
    slidesToScroll: 2,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
$('.reviews-items').slick({
    dots: false,
    infinite: true,
    lazyLoad: 'ondemand',
    speed: 300,
    arrows: true,
    prevArrow: '<button type=\'button\' class=\'slick-prev pull-left\'><i class="fas fa-chevron-left text-2xl text-mdgray"></i></button>',
    nextArrow: '<button type=\'button\' class=\'slick-next pull-right\'><i class="fas fa-chevron-right text-2xl text-mdgray"></i></button>',
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
$('.gallery-items').slick({
    dots: false,
    infinite: false,
    lazyLoad: 'ondemand',
    speed: 300,
    arrows: true,
    prevArrow: '<button type=\'button\' class=\'slick-prev mr-3\'><i class="fas fa-chevron-left text-2xl text-mdgray"></i></button>',
    nextArrow: '<button type=\'button\' class=\'slick-next	\'><i class="fas fa-chevron-right text-2xl text-mdgray"></i></button>',
    slidesToShow: 1,
    slidesToScroll: 1,
});
$('.examples-items').slick({
    dots: false,
    infinite: true,
    lazyLoad: 'ondemand',
    prevArrow: '<button type=\'button\' class=\'slick-prev pull-left\'><i class="fas fa-chevron-left text-2xl text-mdgray"></i></button>',
    nextArrow: '<button type=\'button\' class=\'slick-next pull-right\'><i class="fas fa-chevron-right text-2xl text-mdgray"></i></button>',
    speed: 300,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
$('.park-items').slick({
    dots: false,
    infinite: true,
    lazyLoad: 'ondemand',
    prevArrow: '<button type=\'button\' class=\'slick-prev pull-left\'><i class="fas fa-chevron-left text-2xl text-mdgray"></i></button>',
    nextArrow: '<button type=\'button\' class=\'slick-next pull-right\'><i class="fas fa-chevron-right text-2xl text-mdgray"></i></button>',
    speed: 300,
    arrows: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint:600,
            settings: {
                slidesToShow: 1,
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});
$('.metals-items').slick({
    dots: false,
    infinite: true,
    lazyLoad: 'ondemand',
    prevArrow: '<button type=\'button\' class=\'slick-prev pull-left\'><i class="fas fa-chevron-left text-2xl text-mdgray"></i></button>',
    nextArrow: '<button type=\'button\' class=\'slick-next pull-right\'><i class="fas fa-chevron-right text-2xl text-mdgray"></i></button>',
    speed: 300,
    arrows: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 1300,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint:600,
            settings: {
                slidesToShow: 1,
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});