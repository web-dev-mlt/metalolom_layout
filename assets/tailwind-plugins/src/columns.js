/**
 * Grid
 */

module.exports = ({ addComponents, theme }) => {
  const options = theme('gutenberg')
  const colors = theme('colors');

  const columns = ({
    '.wp-block-columns ': {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'nowrap',
      marginBottom: options.spacing.vertical.default,

      '& .wp-block-column ':{
        flex: '1',
      },

    },

    '[class^=wp-]' : {
      '.slick-slider': {
        display: 'grid',
      },
      'ul':{
        marginTop: '0.5rem',
        marginBottom: '0.5rem',
      },
      'ul li' : {
        fontFamily: theme('fontFamily.regular'),
        fontStyle: 'normal',
        fontWeight: 300,
        listStyleType: 'disc',
      },

      'ul li::marker' : {
        color: colors.mblue,
        fontSize: '1.5rem',
      }

    },
  });



  addComponents([
    columns,
  ])
}
