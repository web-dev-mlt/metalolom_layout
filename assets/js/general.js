// $.event.special.touchstart = {
//   setup: function( _, ns, handle ){
//     if ( ns.includes("noPreventDefault") ) {
//       this.addEventListener("touchstart", handle, { passive: false });
//     } else {
//       this.addEventListener("touchstart", handle, { passive: true });
//     }
//   }
// };
//
// $.event.special.touchmove = {
//   setup: function( _, ns, handle ){
//     if ( ns.includes("noPreventDefault") ) {
//       this.addEventListener("touchmove", handle, { passive: false });
//     } else {
//       this.addEventListener("touchmove", handle, { passive: true });
//     }
//   }
// };

var nav = document.getElementById('site-menu');
var header = document.getElementById('top');

if(nav !== null && header !== null){
  window.addEventListener('scroll', function() {
    if (window.scrollY >=400) { // adjust this value based on site structure and header image height
      nav.classList.add('nav-sticky');
      header.classList.add('pt-scroll');
    } else {
      nav.classList.remove('nav-sticky');
      header.classList.remove('pt-scroll');
    }
  });
}

function navToggle() {
    var btn = document.getElementById('menuBtn');
    var nav = document.getElementById('menu');

    btn.classList.toggle('open');
    nav.classList.toggle('flex');
    nav.classList.toggle('hidden');
}
//INPUT FILE
window.onload = function() {
  if ($(window).width() > 768) {
    let h = $('.wizard > .steps').height();
    $('.wizard > .content').css('height',h);
  }
  else{
    $('.wizard > .content').css('height','auto');
  }


  $( window ).resize(function() {
    if ($(window).width() > 768) {
      let h = $('.wizard > .steps').height();
      $('.wizard > .content').css('height',h);
    }
    else{
      $('.wizard > .content').css('height','auto');
    }
  });
  $('#file-upload').change(function() {
    var i = $(this).prev('label').clone();
    var file = $('#file-upload')[0].files[0].name;
    $(this).prev('label').text(file);
  });
};
//INPUT NUMBER
function decrement(e) {
  const btn = e.target.parentNode.parentElement.parentElement.querySelector(
      'button[data-action="decrement"]'
  );
  let price = $(e.target).parentsUntil('tr').parent().find('td.price-in-table');
  const target = btn.nextElementSibling;
  let value = Number(target.value);
  value--;
  if(value>=1){
    price.text('от '+price.attr('data-price')*value+' руб');
    target.value = value;
  }

}

function increment(e) {
  const btn = e.target.parentNode.parentElement.parentElement.querySelector(
      'button[data-action="decrement"]'
  );
  let price = $(e.target).parentsUntil('tr').parent().find('td.price-in-table');
  const target = btn.nextElementSibling;
  let value = Number(target.value);
  value++;
  price.text('от '+price.attr('data-price')*value+' руб');
  target.value = value;
}

const decrementButtons = document.querySelectorAll(
    `button[data-action="decrement"]`
);

const incrementButtons = document.querySelectorAll(
    `button[data-action="increment"]`
);

decrementButtons.forEach(btn => {
  btn.addEventListener("click", decrement);
});


incrementButtons.forEach(btn => {
  btn.addEventListener("click", increment);
});
