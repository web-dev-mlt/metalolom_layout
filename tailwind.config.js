const colors = require('tailwindcss/colors')
const plugin = require('tailwindcss/plugin');
const _ = require("lodash");

module.exports = {
  darkMode: false,
  prefix: '',
  important: false,
  separator: ':',
  purge: {
    enabled: true,
    content: [
      './layouts/**/*.html',
      './templates/**/*.twig',
      './assets/css/*.css',
      './assets/js/*.js',
      './safelist.txt'
    ],
  },
   plugins: [
    require('./assets/tailwind-plugins/src/block-content'),
    require('./assets/tailwind-plugins/src/block-media-text'),
    require('./assets/tailwind-plugins/src/typography'),
    require('./assets/tailwind-plugins/src/colors'),
    require('./assets/tailwind-plugins/src/aspect-ratios'),
    require('./assets/tailwind-plugins/src/columns'),
    require('./assets/tailwind-plugins/src/group'),
    require('./assets/tailwind-plugins/src/figcaption'),
    require('./assets/tailwind-plugins/src/block-embed'),
    require('./assets/tailwind-plugins/src/block-image'),
    require('./assets/tailwind-plugins/src/block-video'),
    require('./assets/tailwind-plugins/src/block-cover'),
    require('./assets/tailwind-plugins/src/block-table'),
    require('./assets/tailwind-plugins/src/block-gallery'),
    require('@tailwindcss/custom-forms'),
  ],
  theme: {
    fontFamily: {
      regular: ['PF DinDisplay Pro'],
      thin: ['PF DinDisplay Pro ExtraThin'],
      logo: ['StartC']
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
    },
    colors: {
      transparent: 'transparent',

      black: '#000',
      white: '#fff',
      gray: {
        100: '#f7fafc',
        200: '#edf2f7',
        300: '#e2e8f0',
        400: '#9CA3AF',
        500: '#a0aec0',
        600: '#718096',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1a202c',
      },
      blue: {
        100: '#ebf8ff',
        200: '#bee3f8',
        300: '#90cdf4',
        400: '#63b3ed',
        500: '#4299e1',
        600: '#3182ce',
        700: '#2b6cb0',
        800: '#2c5282',
        900: '#2a4365',
      },
      indigo: {
        100: '#ebf4ff',
        200: '#c3dafe',
        300: '#a3bffa',
        400: '#7f9cf5',
        500: '#667eea',
        600: '#5a67d8',
        700: '#4c51bf',
        800: '#434190',
        900: '#3c366b',
      },
      mgray: '#f5f4fa',
      mavgray: '#c5c9d1',
      mdgray: '#b3b3b3',
      mblue: '#2968f8',
      mlblue: '#6a7080',
      mdblue: '#35383f',
      mlgreen:'#3ec512'
    },
    minWidth:{
      'w-40': '10rem'
    },
    extend: {},
    gutenberg: require('./assets/tailwind-plugins/src/default.config'),
  },
  variants: {
    extend: {},
  },
}
